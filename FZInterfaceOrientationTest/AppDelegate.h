//
//  AppDelegate.h
//  FZInterfaceOrientationTest
//
//  Created by Fan Zhang on 6/7/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

