//
//  ViewController.m
//  FZInterfaceOrientationTest
//
//  Created by Fan Zhang on 6/7/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
            

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Traits collection is recommended
//- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection{
//    
//    NSLog(@"%@", NSStringFromCGSize([UIScreen mainScreen].bounds.size));
//    
//}

//This is not recommended
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    NSLog(@"%@", NSStringFromCGSize([UIScreen mainScreen].bounds.size));
    
}

@end
